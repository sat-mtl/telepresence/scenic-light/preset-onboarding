import React, { useContext, useEffect } from 'react'

import { v4 as uuidv4 } from 'uuid'
import { Common, Inputs } from '@sat-valorisation/ui-components'
import { AppStoresContext } from '@components/App'
import { observer } from 'mobx-react-lite'

import '@styles/PresetConnectorSchema.scss'
import { toColor } from '@models/ConnectorEnum'

const { Icon } = Common
const { Checkbox } = Inputs
/**
 * Renders the header of the connection schema
 * @selector `.ConnectionHeader`
 * @param {string} label - The title of the header
 * @returns {external:react/Component} The connection schema header for hdmi and jack inputs
 */
const ConnectionHeader = ({ label }) => {
  return (
    <div className='ConnectionHeader'>
      <span>{label}</span>
    </div>
  )
}

/**
 * Renders a connection schema item or a connector
 * @selector `.ConnectionSchemaItem`
 * @param {Object} connector - The connector object
 * @returns {external:react/Component} The connection schema item
 */
const ConnectionSchemaItem = ({ connector }) => {
  const { connectorStore } = useContext(AppStoresContext)
  const checked = connectorStore.isConnectorSaved(connector.id)
  const type = connector.type.toLowerCase()
  return (
    <div className='ConnectionSchemaItem'>
      <div className='ConnectionSchemaCheckbox'>
        <Checkbox
          key={uuidv4()}
          shape='circle'
          color={toColor(type)}
          size='xlarge'
          checked={checked}
        >
          <div style={{ width: '1rem', height: '0,75rem' }}>
            <Icon type='check' />
          </div>
        </Checkbox>
      </div>
      <label>{connector.id}</label>
    </div>
  )
}

/**
 * Renders the connection schema content
 * @selector `.ConnectionContent`
 * @param {Object} connectors - An array of the displayed connectors
 * @returns {external:react/Component} The connection schema content
 */
const ConnectionContent = ({ connectors, label }) => {
  return (
    <div className='ConnectionContent'>
      <ConnectionHeader label={label} />
      <div className='ConnectionItemsContainers'>
        {connectors.map(
          (connector, i) =>
            <ConnectionSchemaItem
              key={i}
              connector={connector}
            />
        )}
      </div>
    </div>
  )
}

/**
 * Renders the connection schema of the preset
 * @selector `#PresetConnectorSchema`
 * @returns {external:react/Component} The connection schema
 */
const PresetConnectorSchema = observer(() => {
  const { connectorStore, cycleStore } = useContext(AppStoresContext)
  const { inputConnectors, outputConnectors, usbConnectors, isInputConnectorsDisplayed, isOutputConnectorsDisplayed, isUsbConnectorsDisplayed } = connectorStore

  // re-render the component when user is testing to update the connectors status
  useEffect(() => {}, [cycleStore.isTestingDisplayed, cycleStore.isChecklistDisplayed])

  return (
    <section id='PresetConnectorSchema'>
      {isInputConnectorsDisplayed && <ConnectionContent connectors={inputConnectors} label='Inputs' />}
      {isOutputConnectorsDisplayed && <ConnectionContent connectors={outputConnectors} label='Outputs' />}
      {isUsbConnectorsDisplayed && <ConnectionContent connectors={usbConnectors} label='USB and other' />}
    </section>
  )
})

export default PresetConnectorSchema
