import React, { useContext, useState } from 'react'
import { observer } from 'mobx-react-lite'

import { Common, Inputs } from '@sat-valorisation/ui-components'
import '@sat-valorisation/ui-components/ui-components.css'

import { AppStoresContext } from '@components/App'

import { CycleEnum } from '@models/CycleEnum'

import '@styles/App.scss'

const { Button, Icon } = Common
const { InputText, InputSelect } = Inputs

/**
 * @constant {string} DEFAULT_CONFIG_TITLE - Title of the default configuration
 * @memberof ConfigurationSection
 */
export const DEFAULT_CONFIG_TITLE = "Animator's video"

/**
 * Displays the current configuration section of the preset
 * @selector `#ConfigurationSection`
 * @returns {external:mobx-react/ObserverComponent} A configuration section
 */
const ConfigurationSection = observer(() => {
  const { presetStore, connectorStore, cycleStore } = useContext(AppStoresContext)
  const { currentConfiguration, configurationPageNumber, configurationDeviceOptions, currentConfigurationId } = presetStore
  const { configurationConnectorOptions } = connectorStore

  const [config, setConfig] = useState({
    name: 'Enter the device name',
    connectorId: '',
    deviceType: ''
  })

  const setConnectorId = (id) => {
    setConfig({ ...config, connectorId: id })
  }

  const setDeviceType = (id) => {
    setConfig({ ...config, deviceType: id })
  }

  const onSaveHandler = () => {
    cycleStore.setCurrentCycle(CycleEnum.TESTING)
    connectorStore.setSelectedConnectors(currentConfigurationId, config.connectorId)
  }

  return (
    <section id='ConfigurationSection'>
      <div className='ConfigurationSectionContainer'>
        <div className='ConfigurationSectionHeader'>
          <div className='Title'>{currentConfiguration?.title || DEFAULT_CONFIG_TITLE}</div>
          <div className='PageNumber'>{configurationPageNumber}/4</div>
        </div>
        <form className='ConfigurationSectionForm'>
          <InputText
            id='InputText'
            title='Name of the device'
            value={config.name}
            onChange={(e) => { setConfig({ ...config, name: e.target.value }) }}
            size='large'
          />
          <InputSelect
            options={configurationConnectorOptions}
            place='top'
            placeholder='Choose a plug for the device'
            label='Connection'
            addonAfter={<Icon type='dropDown' />}
            onChange={selection => {
              setConnectorId(selection.id)
            }}
          />
          <InputSelect
            options={configurationDeviceOptions}
            place='top'
            placeholder='Choose the type of the device'
            label='Type of the device'
            addonAfter={<Icon type='dropDown' />}
            onChange={selection => {
              setDeviceType(selection.id)
            }}
          />
          <footer className='ConfigurationSectionFormFooter'>
            <Button id='ResetButton' variant='text' size='normal'>Reset</Button>
            <Button id='SaveButton' size='normal' onClick={onSaveHandler}>Save</Button>
          </footer>
        </form>
      </div>
    </section>
  )
})

export default ConfigurationSection
