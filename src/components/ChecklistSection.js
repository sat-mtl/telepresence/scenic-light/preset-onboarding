import React, { useState, useContext } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { Common, Inputs } from '@sat-valorisation/ui-components'

import { AppStoresContext } from '@components/App'

import '@styles/ChecklistSection.scss'

const { Icon } = Common
const { Checkbox } = Inputs

/**
 * Renders the pre-event checklist section item for the preset
 * @param {Object} item - An item on the checklist
 * @returns {external:react/Component} The checklist section item
 */
const ChecklistItem = ({ item }) => {
  const [isChecked, setIsChecked] = useState(false)

  const toggleCheck = () => {
    setIsChecked(!isChecked)
  }
  return (
    <li key={uuidv4()}>
      <Checkbox
        checked={isChecked}
        onChange={toggleCheck}
      >
        <Icon type='check' />
      </Checkbox>
      <span>{item}</span>
    </li>
  )
}

/**
 * Renders the pre-event checklist section for the preset
 * @selector `#ChecklistSection`
 * @returns {external:react/Component} The checklist section
 */
const ChecklistSection = () => {
  const { presetStore: { checkList } } = useContext(AppStoresContext)
  return (
    <section id='ChecklistSection'>
      <p>Verify that you have prepared the items below before starting the event</p>
      <ul>
        {checkList?.map((item) => <ChecklistItem key={uuidv4()} item={item} />)}
      </ul>
    </section>
  )
}

export default ChecklistSection
