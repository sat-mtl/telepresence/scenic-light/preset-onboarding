import React, { useContext } from 'react'
import { AppStoresContext } from '@components/App'

import '@styles/StepsSection.scss'
import background0 from '@assets/img/black-background.jpg'
import { observer } from 'mobx-react-lite'

/**
 * Renders the steps section header
 * @selector `#StepsSectionHeader`
 * @returns {external:react/Component} The steps section header
 */
const StepsSectionHeader = () => {
  return (
    <p className='StepsSectionHeader'>
      Here are the steps that you need to follow for this preset
    </p>
  )
}

/**
 * Renders a steps section item
 * @selector `#StepsSectionItem`
 * @returns {external:react/Component} A step of the steps section
 */
const StepsSectionItem = ({ step, index }) => {
  return (
    <li id='StepsSectionItem' key={step.id}>
      <div className='StepsSectionItemThumbnail'>
        <img className='StepsSectionItemImg' alt='thumbnail' src={background0} />
      </div>
      <div className='StepsSectionItemDescription'>
        <p className='StepsSectionItemDescriptionHeader'>
          {step.title}
          <span className='StepsSectionItemDescriptionText'>
            {step.description}
          </span>
        </p>
      </div>
      <span className='StepsSectionItemDescriptionPageCounter'>{step.totalPageCount}</span>
    </li>
  )
}

/**
 * Renders the steps of the preset
 * @selector `#StepsSection`
 * @returns {external:react/Component} The preset steps section
 */
const StepsSection = observer(() => {
  const { stepsStore } = useContext(AppStoresContext)
  return (
    <section id='StepsSection'>
      <div style={{ width: '100%' }}>
        <StepsSectionHeader />
        <ul>
          {stepsStore.stepsOptions.map((step, index) =>
            <StepsSectionItem step={step} index={index} key={step.id} />
          )}
        </ul>
      </div>
    </section>
  )
})

export default StepsSection
