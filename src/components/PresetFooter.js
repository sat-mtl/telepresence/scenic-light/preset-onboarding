import React, { useContext } from 'react'

import { observer } from 'mobx-react-lite'
import { Common } from '@sat-valorisation/ui-components'

import { AppStoresContext } from '@components/App'
import { CycleEnum } from '@models/CycleEnum'

import '@styles/PresetFooter.scss'

const { Button, Icon } = Common

/**
 * Renders a return button
 * @selector `#ReturnButton`
 * @returns {external:react/Component} The return button of the preset footer
 */
const ReturnButton = ({ onChangeHandler, label }) => {
  return (
    <Button
      id='ReturnButton'
      shape='rectangle'
      variant='text'
      size='large'
      onClick={onChangeHandler}
    >
      <span className='BackSummaryIcon'>
        <Icon type='right' />
      </span>
      {label}
    </Button>
  )
}

/**
 * Renders a forward button
 * @selector `#ForwardButton`
 * @returns {external:react/Component} The forward button of the preset footer
 */
const ForwardButton = ({ onChangeHandler, label }) => {
  return (
    <Button
      id='ForwardButton'
      shape='rectangle'
      variant='text'
      size='large'
      onClick={onChangeHandler}
    >
      {label}
      <span className='ConfigureOutputsIcon'>
        <Icon type='right' />
      </span>
    </Button>
  )
}

/**
 * Renders the footer of the preset steps page (first page)
 * @selector `.PresetStartFooter`
 * @returns {external:react/Component} The start preset footer
 */
const PresetStartFooter = () => {
  const { cycleStore, presetStore } = useContext(AppStoresContext)
  const onConfirm = () => {
    cycleStore.setCurrentCycle(CycleEnum.CONFIGURING)
    presetStore.setCurrentConfigurationId('animatorVideo')
  }

  return (
    <footer className='PresetStartFooter'>
      <Button id='CancelButton' variant='text' size='large'>Cancel</Button>
      <Button id='ConfirmButton' size='large' onClick={onConfirm}>Start</Button>
    </footer>
  )
}

/**
 * Renders the footer of the preset configuration workflow
 * @selector `.PresetConfigurationFooter`
 * @returns {external:react/Component} The configuration workflow preset footer
 * @todo: Change icon types when scenicLight icons are added to the ui-components library
 */
const ConfigurationFooter = observer(() => {
  const { presetStore, sceneAdviceStore, cycleStore } = useContext(AppStoresContext)
  const { currentEntryType } = presetStore
  const { preset } = presetStore

  const onConfigureOutputsHandler = () => {
    cycleStore.setCurrentCycle(CycleEnum.CONFIGURING)
    presetStore.setCurrentConfigurationId(preset?.allOutputIds[0])
    sceneAdviceStore.setCurrentSceneAdviceId(preset?.allOutputIds[0])
  }

  const onSummaryHandler = () => {
    cycleStore.setCurrentCycle(CycleEnum.INITIALIZING)
    presetStore.setCurrentConfigurationId('')
    sceneAdviceStore.setCurrentSceneAdviceId(preset?.allInputIds[0])
  }

  const onInputConfigHandler = () => {
    presetStore.setCurrentConfigurationId(preset?.allInputIds[0])
    sceneAdviceStore.setCurrentSceneAdviceId(preset?.allInputIds[0])
  }

  const onChecklistHandler = () => {
    cycleStore.setCurrentCycle(CycleEnum.CHECKING)
  }

  return (
    <footer className='PresetConfigurationFooter'>
      <ReturnButton
        label={currentEntryType === 'input' ? 'Back to Summary' : 'Back to Input Configuration'}
        onChangeHandler={currentEntryType === 'input' ? onSummaryHandler : onInputConfigHandler}
      />
      <ForwardButton
        label={currentEntryType === 'input' ? 'Configure Outputs' : 'Pre-event Checklist'}
        onChangeHandler={currentEntryType === 'input' ? onConfigureOutputsHandler : onChecklistHandler}
      />
    </footer>
  )
})

/**
 * Renders the footer of the checklist page (last page)
 * @selector `.ChecklistFooter`
 * @returns {external:react/Component} The configuration workflow preset footer
 * @todo: Change icon types when scenicLight icons are added to the ui-components library
 */
const ChecklistFooter = () => {
  const { presetStore, sceneAdviceStore, cycleStore } = useContext(AppStoresContext)
  const { preset } = presetStore

  const onConfirm = () => {}

  const onOutputConfigHandler = () => {
    cycleStore.setCurrentCycle(CycleEnum.CONFIGURING)
    presetStore.setCurrentConfigurationId(preset?.allOutputIds[0])
    sceneAdviceStore.setCurrentSceneAdviceId(preset?.allOutputIds[0])
  }

  return (
    <footer className='ChecklistFooter'>
      <ReturnButton
        label='Back to Output Configuration'
        onChangeHandler={onOutputConfigHandler}
      />
      <Button id='ConfirmButton' size='large' onClick={onConfirm}>Begin with preset</Button>
    </footer>
  )
}

/**
 * Renders the footer of the preset
 * @selector `#PresetFooter`
 * @returns {external:react/Component} The preset footer
 */
const PresetFooter = () => {
  const { cycleStore } = useContext(AppStoresContext)
  const { isConfigurationDisplayed, isTestingDisplayed, isChecklistDisplayed } = cycleStore
  return (
    <div id='PresetFooter'>
      {(!isConfigurationDisplayed && !isTestingDisplayed && !isChecklistDisplayed) && (
        <PresetStartFooter />
      )}

      {(isConfigurationDisplayed || isTestingDisplayed) && <ConfigurationFooter />}

      {(isChecklistDisplayed) && <ChecklistFooter />}
    </div>
  )
}

export default PresetFooter
