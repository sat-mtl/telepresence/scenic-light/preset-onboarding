import React, { useContext } from 'react'
import { AppStoresContext } from '@components/App'
import '@styles/DeviceListSection.scss'

/**
 * Renders the preset devices list section
 * @selector `#DeviceListSection`
 * @returns {external:react/Component} A section that lists the required devices for the preset
 */
const DeviceListSection = () => {
  const { presetStore: { deviceList } } = useContext(AppStoresContext)
  return (
    <div id='DeviceListSection'>
      <h4 className='DeviceListTitle'>To install this model, you will need at least:</h4>
      <ul>
        {deviceList?.map((item, i) => <li key={i}>{item}</li>)}
      </ul>
    </div>
  )
}

export default DeviceListSection
