import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import { Context } from '@sat-valorisation/ui-components'

import ConfigurationSection from '@components/ConfigurationSection'
import PresetSceneSchema from '@components/PresetSceneSchema'
import PresetConnectorSchema from '@components/PresetConnectorSchema'
import PresetHeader from '@components/PresetHeader'
import PresetFooter from '@components/PresetFooter'
import StepsSection from '@components/StepsSection'
import ChecklistSection from '@components/ChecklistSection'
import { PresetTitleEnum } from '@models/PresetEnum'
import SceneAdviceSection from '@components/SceneAdviceSection'
import DeviceListSection from '@components/DeviceListSection'
import { AppStoresContext } from '@components/App'

const { ThemeContext } = Context

/**
 * Renders the page layout of the preset
 * @selector `#PageLayout`
 * @returns {external:react/Component} The page layout for preset workflow
 */
const PageLayout = observer(() => {
  const theme = useContext(ThemeContext)
  const { cycleStore } = useContext(AppStoresContext)
  const { isStepsDisplayed, isConfigurationDisplayed, isTestingDisplayed, isConnectorDisplayed, isChecklistDisplayed } = cycleStore

  return (
    <div id='PageLayout' className={`Page-${theme}`}>
      <PresetHeader title={PresetTitleEnum.SMALL_HALLS} />
      <main>
        <PresetSceneSchema />
        {isStepsDisplayed && <StepsSection />}
        {isTestingDisplayed && <SceneAdviceSection />}
        {isConfigurationDisplayed && <ConfigurationSection />}
        {isChecklistDisplayed && <ChecklistSection />}
        {isConnectorDisplayed && <PresetConnectorSchema />}
        {isStepsDisplayed && <DeviceListSection />}
      </main>
      <PresetFooter />
    </div>
  )
})

export default PageLayout
