import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import classNames from 'classnames'

import { Common } from '@sat-valorisation/ui-components'

import { AppStoresContext } from '@components/App'
import SchemaConnector from '@components/SchemaConnector'

import { toColor } from '@models/ConnectorEnum'

import '@styles/PresetSceneSchema.scss'

const { Icon } = Common

/**
 * Renders a pin in a scene schema group
 * @param {Object} group - An object group containing all group details
 * @param {string} configId - The id of a group
 * @param {string} iconType - The type of the pin icon
 * @returns {external:react/Component} The Pin of a scene schema pin group
 */
const Pin = observer(({ configId, group, iconType }) => {
  const { sceneStore, connectorStore } = useContext(AppStoresContext)
  const iconTypeClass = `icon-${iconType}`

  let $pin = ''

  if (sceneStore.isConnectedPin(configId)) {
    const connectorLabel = connectorStore.selectedConnectors.get(configId)
    $pin = (
      <SchemaConnector
        isActive={sceneStore.isTestingConnectedPin(configId)}
        color={toColor(connectorStore.setConnectorType(configId))}
      >
        {connectorLabel}
      </SchemaConnector>
    )
  } else if (sceneStore.isActivePin(configId)) {
    $pin = (
      <Icon type={`${sceneStore.pinIconTypes.get(configId)}Active`} />
    )
  } else {
    $pin = (
      <Icon type={sceneStore.pinIconTypes.get(configId)} />
    )
  }

  return (
    <div style={{ transform: `${group?.transformation}` }} className={classNames(`icon ${iconTypeClass}`)}>
      {$pin}
    </div>
  )
})

/**
 * Renders a pin group of the scene schema
 * @selector `.PinGroup`
 * @param {Object} group - An object group containing all group details
 * @returns {external:react/Component} The Pin group
 */
const PinGroup = observer(({ group }) => {
  const { sceneStore } = useContext(AppStoresContext)
  let $group = ''

  $group = (
    <div className='PinGroup' style={{ top: group.position.top, right: group.position.right }}>
      {group.label && <span className='PinGroupTitle'>{group.label}</span>}
      <div className='PinGroupIcons'>
        {group.iconTypes.map(item =>
          <Pin
            key={item.id}
            configId={item.id}
            iconType={item.type}
            group={group}
          />)}
      </div>
    </div>
  )

  return <>{sceneStore.isDisplayedGroup(group.id) && $group}</>
})

/**
 * Renders the scene schema of the preset
 * @selector `#PresetSceneSchema`
 * @returns {external:react/Component} The scene schema
 */
const PresetSceneSchema = observer(() => {
  const { sceneStore } = useContext(AppStoresContext)
  return (
    <section id='PresetSceneSchema'>
      <p className='SceneSchemaTitle'>Summary</p>
      <div className='SceneSchemaContainer' style={{ width: '100%' }}>
        {Array.from(sceneStore.availablePinGroups.values()).map(group =>
          <PinGroup key={group.id} group={group} />
        )}
      </div>
    </section>
  )
})

export default PresetSceneSchema
