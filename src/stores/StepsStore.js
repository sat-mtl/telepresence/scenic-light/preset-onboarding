import Step from '@models/Step'
import { makeObservable, observable, action } from 'mobx'

/**
 * @classdesc Stores all presets
 * @memberof stores
 */
class StepsStore {
  /** @property {Set<Step>} presetSteps - A set of every step that should be done for the preset */
  presetSteps = new Set()

  constructor () {
    makeObservable(this, {
      presetSteps: observable,
      setPresetSteps: action
    })
  }

  /**
   * Gets the list of all steps for the preset
   * @returns {Object} options - Array of all the available steps
   */
  get stepsOptions () {
    return [...this.presetSteps]
  }

  /**
  * Creates a preset steps model from a JSON object
  * @param {Object} json - JSON representation of the preset steps list
  * @returns {module:models/preset} A preset steps model
  */
  makePresetStepsModel (json) {
    let presetSteps = null

    try {
      presetSteps = Step.fromSteps(json)
    } catch (error) {
      console.error(`Failed to make a preset steps model from ${json}`)
    }
    return presetSteps
  }

  /**
   * Sets the new preset steps list
   * @param {Object} preset - A set of the steps rewuired for the preset
   */
  setPresetSteps (presetSteps) {
    this.presetSteps = new Set(presetSteps)
  }
}

export default StepsStore
