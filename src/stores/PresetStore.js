import Preset from '@models/Preset'
import { makeObservable, observable, action, computed } from 'mobx'

import { EntryTypeEnum } from '@models/EntryTypeEnum'
import { ConfigurationIdEnum } from '@models/PresetEnum'

import CycleStore from '@stores/CycleStore'

const {
  VIDEO_ANIMATOR,
  AUDIO_ANIMATOR,
  VIDEO_AUDIENCE,
  AUDIO_AUDIENCE,
  LEFT_SCREEN,
  RIGHT_SCREEN,
  LEFT_SPEAKER,
  RIGHT_SPEAKER
} = ConfigurationIdEnum

/**
 * @classdesc Stores all presets
 * @memberof stores
 */
class PresetStore {
  preset = null
  currentConfigurationId = ''

  constructor (cycleStore) {
    if (cycleStore instanceof CycleStore) {
      this.cycleStore = cycleStore
    } else {
      throw new TypeError('CycleStore is required')
    }

    makeObservable(this, {
      preset: observable,
      setPreset: action,

      currentConfigurationId: observable,
      setCurrentConfigurationId: action,

      currentConfiguration: computed,
      currentEntryType: computed,
      configurationDirections: computed
    })
  }

  /**
   * Gets the configuration directions map
   */
  get configurationDirections () {
    const configurationDirections = new Map()

    if (this.preset) {
      const { input, output } = this.preset

      for (const resource of input.resources) {
        configurationDirections.set(resource.id, 'input')
      }

      for (const resource of output.resources) {
        configurationDirections.set(resource.id, 'output')
      }
    }

    return configurationDirections
  }

  /**
   * Checks if the entry that is being tested is a video input
   * @returns {boolean} Returns true if the tested entry is a video input
   */
  get isVideoInputTested () {
    return [VIDEO_ANIMATOR, VIDEO_AUDIENCE].includes(this.currentConfigurationId)
  }

  /**
   * Checks if the entry that is being tested is a audio input
   * @returns {boolean} Returns true if the tested entry is a audio input
   */
  get isAudioInputTested () {
    return [AUDIO_ANIMATOR, AUDIO_AUDIENCE].includes(this.currentConfigurationId)
  }

  /**
   * Checks if the entry that is being tested is an output
   * @returns {boolean} Returns true if the tested entry is an output
   */
  get isOutputTested () {
    return [LEFT_SPEAKER, RIGHT_SPEAKER, LEFT_SCREEN, RIGHT_SCREEN].includes(this.currentConfigurationId)
  }

  /**
   * Gets the current direction of
   * @returns {string} currentDirection - The current direction of a configuration
   */
  get currentDirection () {
    return this.configurationDirections.get(this.currentConfigurationId)
  }

  /**
   * Gets the device options of the current configuration select component
   * @returns {Object} options - Array of all the available options
   */
  get configurationDeviceOptions () {
    let options = []

    switch (this.currentConfigurationId) {
      case ('animatorVideo'):
      case ('audienceVideo'):
        options = this.preset?.inputVideoDevices
        break
      case ('animatorAudio'):
      case ('audienceAudio'):
        options = this.preset?.inputAudioDevices
        break
      case ('leftScreen'):
      case ('rightScreen'):
        options = this.preset?.outputVideoDevices
        break
      case ('leftSpeaker'):
      case ('rightSpeaker'):
        options = this.preset?.outputAudioDevices
        break
      default:
        options = []
        break
    }
    return options
  }

  /**
   * Gets the page number of the current configuration
   * @returns {string} - The number of the current page configuration
   */
  get configurationPageNumber () {
    switch (this.currentConfigurationId) {
      case ('animatorVideo'):
      case ('leftScreen'):
        return '1'
      case ('animatorAudio'):
      case ('leftSpeaker'):
        return '2'
      case ('audienceVideo'):
      case ('rightScreen'):
        return '3'
      case ('audienceAudio'):
      case ('rightSpeaker'):
        return '4'
      default:
        return '0'
    }
  }

  /** @property {Object} CurrentConfiguration - The current configuration of the preset */
  get currentConfiguration () {
    let currentConfiguration = null

    if (this.preset) {
      const { input, output } = this.preset

      for (const resource of [...output.resources, ...input.resources]) {
        if (resource.id === this.currentConfigurationId) {
          currentConfiguration = resource
        }
      }
    }

    return currentConfiguration
  }

  /** @property {Object} - The list of devices required for the preset */
  get deviceList () {
    return this.preset?.deviceList
  }

  /** @property {Object} - The checklist required for the preset */
  get checkList () {
    return this.preset?.checkList
  }

  /**
   * Gets the label of the next button in the media testing container
   * @returns {string} label - The label of the next button
   */
  get testButtonLabel () {
    let label = ''

    const entries = this.preset?.allEntriesIds
    const inputs = this.preset?.allInputIds
    const outputs = this.preset?.allOutputIds
    const id = this.currentConfigurationId

    for (const entry of entries) {
      if (entry === id && this.preset?.isInput(entry) && inputs.indexOf(entry) !== inputs.length - 1) {
        label = 'Next Input'
      } else if (entry === id && this.preset?.isOutput(entry) && outputs.indexOf(entry) !== outputs.length - 1) {
        label = 'Next Output'
      }
    }
    return label
  }

  /** @property {string} currentEntryType - The current entry type of the preset */
  get currentEntryType () {
    let currentEntryType = EntryTypeEnum.INPUT

    if (this.preset) {
      const { output } = this.preset

      if (output.resources.find(item => item.id === this.currentConfigurationId)) {
        currentEntryType = EntryTypeEnum.OUTPUT
      }
    }

    return currentEntryType
  }

  /**
   * Gets the id of the next configuration section
   * @returns {string} nextConfigurationId - The id of the next configuration
   */
  get nextConfigurationId () {
    let nextConfigurationId = ''

    if (this.preset) {
      const inputs = this.preset.allInputIds
      const outputs = this.preset.allOutputIds
      const id = this.currentConfigurationId

      if (this.preset?.isInput(id) && inputs.indexOf(id) !== inputs.length - 1) {
        const currentIndex = inputs.findIndex(itemId => id === itemId)
        nextConfigurationId = inputs[currentIndex + 1]
      } else if (this.preset?.isOutput(id)) {
        const currentIndex = outputs.findIndex(itemId => id === itemId)
        nextConfigurationId = outputs[currentIndex + 1]
      }
    }
    return nextConfigurationId
  }

  /**
  * Creates a preset model from a JSON object
  * @param {Object} json - JSON representation of the preset
  * @returns {module:models/preset} A preset model
  */
  makePresetModel (json) {
    let preset = null

    try {
      preset = Preset.fromJSON(json)
    } catch (error) {
      console.error(`Failed to make a preset model from ${json}`)
    }
    return preset
  }

  /**
   * Sets the new preset
   * @param {Object} preset - The preset model
   */
  setPreset (preset) {
    this.preset = preset
  }

  /**
   * Sets the new configuration Id
   * @param {string} itemId - The Id of the current configuration
   */
  setCurrentConfigurationId (itemId) {
    this.currentConfigurationId = itemId
  }
}

export default PresetStore
