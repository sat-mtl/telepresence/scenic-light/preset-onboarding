import React from 'react'
import { makeObservable, observable, action, computed } from 'mobx'
import Connector from '@models/Connector'

import CycleStore from '@stores/CycleStore'
import PresetStore from '@stores/PresetStore'

import JsonConnectors from '@assets/json/connectors.json'

import { Common } from '@sat-valorisation/ui-components'
const { Icon } = Common

/**
 * @classdesc Stores all connectors
 * @memberof stores
 */
class ConnectorStore {
  availableConnectors = new Set(Connector.fromArray(JsonConnectors))

  /** @property {Map<string, string>} selectedConnectors - All connector's ID hashed by each configuration ID */
  selectedConnectors = new Map()

  constructor (presetStore, cycleStore) {
    if (presetStore instanceof PresetStore) {
      this.presetStore = presetStore
    } else {
      throw new TypeError('PresetStore is required')
    }

    if (cycleStore instanceof CycleStore) {
      this.cycleStore = cycleStore
    } else {
      throw new TypeError('CycleStore is required')
    }

    makeObservable(this, {
      selectedConnectors: observable,
      setSelectedConnectors: action,

      availableInputVideoConnectors: computed,
      availableInputAudioConnectors: computed
    })
  }

  /**
   * Gets the input connectors from all the available connectors
   * @returns {Object} inputConnectors - Array of all the available input connectors
   */
  get inputConnectors () {
    return [...this.availableConnectors].filter(c => c.direction.includes('input') && !['NDI', 'Bluetooth', 'USB'].includes(c.type))
  }

  /**
   * Gets the output connectors from all the available connectors
   * @returns {Object} outputConnectors - Array of all the available output connectors
   */
  get outputConnectors () {
    return [...this.availableConnectors].filter(c => c.direction.includes('output') && !['NDI', 'Bluetooth', 'USB'].includes(c.type))
  }

  /**
   * Gets the usb connectors from all the available connectors
   * @returns {Object} usbConnectors - Array of all the available usb connectors
   */
  get usbConnectors () {
    return [...this.availableConnectors].filter(c => c.direction.includes('input') && ['USB'].includes(c.type))
  }

  get isInputConnectorsDisplayed () {
    return (this.presetStore.currentEntryType === 'input' || this.cycleStore.isChecklistDisplayed)
  }

  get isOutputConnectorsDisplayed () {
    return (this.presetStore.currentEntryType === 'output' || this.cycleStore.isChecklistDisplayed)
  }

  get isUsbConnectorsDisplayed () {
    return (['input', 'output'].includes(this.presetStore.currentEntryType) || this.cycleStore.isChecklistDisplayed)
  }

  /**
   * Gets the input video connectors from all the available connectors
   * @returns {Object} inputVideoConnectors - Array of all the available input video connectors
   */
  get inputVideoConnectors () {
    return [...this.availableConnectors].filter(c => c.direction.includes('input') && c.media.includes('video'))
  }

  /**
   * Gets the output video connectors from all the available connectors
   * @returns {Object} outputVideoConnectors - Array of all the available output video connectors
   */
  get outputVideoConnectors () {
    return [...this.availableConnectors].filter(c => c.direction.includes('output') && c.media.includes('video'))
  }

  /**
   * Gets the input audio connectors from all the available connectors
   * @returns {Object} inputAudioConnectors - Array of all the available input audio connectors
   */
  get inputAudioConnectors () {
    return [...this.availableConnectors].filter(c => c.direction.includes('input') && c.media.includes('audio'))
  }

  /**
   * Gets the output audio connectors from all the available connectors
   * @returns {Object} outputAudioConnectors - Array of all the available output audio connectors
   */
  get outputAudioConnectors () {
    return [...this.availableConnectors].filter(c => c.direction.includes('output') && c.media.includes('audio'))
  }

  /**
   * Sets the type of a connector
   * @param {string} configId - The id of the configuration
   * @returns {string} type - The type of the connector linked to a specific configuration Id
   */
  setConnectorType (configId) {
    let type = null
    const connectorId = this.selectedConnectors.get(configId)
    for (const connector of this.availableConnectors) {
      if (connector.id === connectorId) type = connector.type.toLowerCase()
    }
    return type
  }

  /**
   * Gets the remaining options for the input video connectors
   * @returns {Object} options - Array of all the available input video connectors options
  */
  get availableInputVideoConnectors () {
    const remainingOptions = []
    for (const connector of this.inputVideoConnectors) {
      if (!Array.from(this.selectedConnectors.values()).includes(connector.id)) remainingOptions.push(connector)
    }
    return remainingOptions
  }

  /**
   * Gets the remaining options for the input audio connectors
   * @returns {Object} options - Array of all the available input audio connectors options
  */
  get availableInputAudioConnectors () {
    const remainingOptions = []
    for (const connector of this.inputAudioConnectors) {
      if (!Array.from(this.selectedConnectors.values()).includes(connector.id)) remainingOptions.push(connector)
    }
    return remainingOptions
  }

  /**
   * Gets the remaining options for the output video connectors
   * @returns {Object} options - Array of all the available output video connectors options
  */
  get availableOutputVideoConnectors () {
    const remainingOptions = []
    for (const connector of this.outputVideoConnectors) {
      if (!Array.from(this.selectedConnectors.values()).includes(connector.id)) remainingOptions.push(connector)
    }
    return remainingOptions
  }

  /**
   * Gets the remaining options for the output audio connectors
   * @returns {Object} options - Array of all the available output audio connectors options
  */
  get availableOutputAudioConnectors () {
    const remainingOptions = []
    for (const connector of this.outputAudioConnectors) {
      if (!Array.from(this.selectedConnectors.values()).includes(connector.id)) remainingOptions.push(connector)
    }
    return remainingOptions
  }

  /**
   * Populates the options for the input video connectors
   * @returns {Object} options - Array of all the input video connectors options
   */
  populateInputVideoConnectors () {
    const options = this.availableInputVideoConnectors.map(option => ({
      id: option.id,
      label: () => (
        <span className={`${option.type.toLowerCase()}-label Label`}>
          {!['NDI', 'B1'].includes(option.id) && option.id}
          {option.id === 'NDI' && <Icon type='ndi' />}
          {option.id === 'B1' && <Icon type='bluetooth' />}
        </span>
      ),
      value: option.type
    }))
    return options
  }

  /**
   * Populates the options for the output video connectors
   * @returns {Object} options - Array of all the output video connectors options
   */
  populateOutputVideoConnectors () {
    const options = this.availableOutputVideoConnectors.map(option => ({
      id: option.id,
      label: () => (
        <span className={`${option.type.toLowerCase()}-label Label`}>
          {!['NDI', 'B1'].includes(option.id) && option.id}
          {option.id === 'NDI' && <Icon type='ndi' />}
          {option.id === 'B1' && <Icon type='bluetooth' />}
        </span>
      ),
      value: option.type
    }))

    return options
  }

  /**
   * Populates the options for the input audio connectors
   * @returns {Object} options - Array of all the input audio connectors options
  */
  populateInputAudioConnectors () {
    const options = this.availableInputAudioConnectors.map(option => ({
      id: option.id,
      label: () => (
        <span className={`${option.type.toLowerCase()}-label Label`}>
          {!['NDI', 'B1'].includes(option.id) && option.id}
          {option.id === 'NDI' && <Icon type='ndi' />}
          {option.id === 'B1' && <Icon type='bluetooth' />}
        </span>
      ),
      value: option.type
    }))
    return options
  }

  /**
   * Populates the options for the output audio connectors
   * @returns {Object} options - Array of all the output audio connectors options
  */
  populateOutputAudioConnectors () {
    const options = this.availableOutputAudioConnectors.map(option => ({
      id: option.id,
      label: () => (
        <span className={`${option.type.toLowerCase()}-label Label`}>
          {!['NDI', 'B1'].includes(option.id) && option.id}
          {option.id === 'NDI' && <Icon type='ndi' />}
          {option.id === 'B1' && <Icon type='bluetooth' />}
        </span>
      ),
      value: option.type
    }))

    return options
  }

  /**
   * Gets the connection options of the current configuration select component
   * @returns {Object} options - Array of all the available options
   */
  get configurationConnectorOptions () {
    const { currentConfigurationId } = this.presetStore

    let options = []

    switch (currentConfigurationId) {
      case ('animatorVideo'):
      case ('audienceVideo'):
        options = this.populateInputVideoConnectors()
        break
      case ('animatorAudio'):
      case ('audienceAudio'):
        options = this.populateInputAudioConnectors()
        break
      case ('leftScreen'):
      case ('rightScreen'):
        options = this.populateOutputVideoConnectors()
        break
      case ('leftSpeaker'):
      case ('rightSpeaker'):
        options = this.populateOutputAudioConnectors()
        break
      default:
        options = []
        break
    }

    return options
  }

  /**
   * Checks if a connector is saved or not
   * @param {string} connectorId - The id of the connector
   * @returns {Boolean} inputConnectors - Returns true if the connector is saved
   */
  isConnectorSaved (connectorId) {
    let isSaved = false
    for (const id of this.selectedConnectors.values()) {
      if (id === connectorId) { isSaved = true }
    }
    return isSaved
  }

  /**
  * Sets the selected connectors
  * @param {string} connectorId - The Id of the connector
  */
  setSelectedConnectors (configId, connectorId) {
    this.selectedConnectors.set(configId, connectorId)
  }
}

export default ConnectorStore
