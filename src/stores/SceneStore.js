import { makeObservable, observable, computed } from 'mobx'

import PresetStore from '@stores/PresetStore'
import ConnectorStore from '@stores/ConnectorStore'
import CycleStore from '@stores/CycleStore'

import Scene from '@models/Scene'
import { CycleEnum } from '@models/CycleEnum'

import JsonScenes from '@assets/json/scenes.json'

/**
 * @classdesc Stores all scenes
 * @memberof stores
 */
class SceneStore {
  availableScenes = new Set(Scene.fromArray(JsonScenes))
  currentSceneId = ''

  /** Map of all groups hashed by groupId: each group provide its label and the type of direction it is associated (inputs or outputs) */
  pinGroups = new Map()

  constructor (presetStore, connectorStore, cycleStore) {
    if (presetStore instanceof PresetStore) {
      this.presetStore = presetStore
    } else {
      throw new TypeError('PresetStore is required')
    }

    if (connectorStore instanceof ConnectorStore) {
      this.connectorStore = connectorStore
    } else {
      throw new TypeError('ConnectorStore is required')
    }

    if (cycleStore instanceof CycleStore) {
      this.cycleStore = cycleStore
    } else {
      throw new TypeError('CycleStore is required')
    }

    makeObservable(this, {
      pinGroups: observable,
      availableScenes: observable,

      pinIconTypes: computed,
      availablePinGroups: computed
    })
  }

  /**
   * Gets the pin groups icon types
   */
  get pinIconTypes () {
    const iconTypes = new Map()
    for (const group of this.availableScenes) {
      group.iconTypes.forEach(icon => iconTypes.set(icon.id, icon.type))
    }
    return iconTypes
  }

  /**
   * Gets the available pin groups of the scene schema
   */
  get availablePinGroups () {
    const availablePinGroups = new Map()
    for (const group of this.availableScenes) {
      availablePinGroups.set(group.id, group)
    }
    return availablePinGroups
  }

  /**
   * Checks if the a pin with a given id is connected and is being tested
   * @returns {boolean} Returns true if the pin's id is found in the selected connectors map and if the user is testing it
   */
  isTestingConnectedPin (configId) {
    return this.cycleStore.currentCycle === CycleEnum.TESTING && this.presetStore.currentConfigurationId === configId
  }

  /**
   * Checks if the a pin with a given id is connected
   * @returns {boolean} Returns true if the pin's id is found in the selected connectors map
   */
  isConnectedPin (configId) {
    return this.connectorStore.selectedConnectors.has(configId)
  }

  /**
   * Checks if the a pin with a given id is active
   * @returns {boolean} Returns true if the pin's id is the same as the current configuration id
   */
  isActivePin (configId) {
    return this.presetStore.currentConfigurationId === configId
  }

  /**
   * Checks if the a scene schema pin group is displayed
   * @param {string} groupId - The id of the pin group
   * @returns {boolean} Returns true if the scene schema should be displayed
   */
  isDisplayedGroup (groupId) {
    const group = this.availablePinGroups.get(groupId)

    const { currentDirection } = this.presetStore

    let isDisplayed = false

    for (const direction of group.direction) {
      if (this.cycleStore.currentCycle === CycleEnum.INITIALIZING ||
        direction === currentDirection ||
        this.cycleStore.currentCycle === CycleEnum.CHECKING
      ) isDisplayed = true
    }
    return isDisplayed
  }
}

export default SceneStore
