import { makeObservable, observable, action, computed } from 'mobx'
import { CycleEnum } from '@models/CycleEnum'

/**
 * @classdesc Stores all cycles
 * @memberof stores
 */
class CycleStore {
  /** @property {string} currentCycle - The current cycle of the app */
  currentCycle = CycleEnum.INITIALIZING

  /** @property {boolean} isConfiguring - Flags true if the user is configuring an entry */
  get isConfiguring () {
    return this.currentCycle === CycleEnum.CONFIGURING
  }

  /** @property {boolean} isTesting - Flags true if the user is testing a device */
  get isTesting () {
    return this.currentCycle === CycleEnum.TESTING
  }

  /** @property {boolean} isConfiguring - Flags true if the user is configuring an entry */
  get isChecking () {
    return this.currentCycle === CycleEnum.CHECKING
  }

  /** @property {boolean} isStepsDisplayed - Flags true if the steps section should be displayed */
  get isStepsDisplayed () {
    return !this.isTesting && !this.isConfiguring && !this.isChecking
  }

  /** @property {boolean} isConfigurationDisplayed - Flags true if the configuration section should be displayed */
  get isConfigurationDisplayed () {
    return !this.isTesting && this.isConfiguring
  }

  /** @property {boolean} isTestingDisplayed - Flags true if the scene advice (testing) section should be displayed */
  get isTestingDisplayed () {
    return this.isTesting && !this.isConfiguring
  }

  /** @property {boolean} isConnectorDisplayed - Flags true if the connector schema should be displayed */
  get isConnectorDisplayed () {
    return this.isTesting || this.isConfiguring || this.isChecking
  }

  /** @property {boolean} isChecklistDisplayed - Flags true if the preset checklist should be displayed */
  get isChecklistDisplayed () {
    return this.isChecking
  }

  constructor () {
    makeObservable(this, {
      currentCycle: observable,
      setCurrentCycle: action,
      isConfiguring: computed,
      isTesting: computed
    })
  }

  /**
  * Sets the current cycle of the app
  * @param {string} value - The current cycle value
  */
  setCurrentCycle (value) {
    this.currentCycle = value
  }
}

export default CycleStore
