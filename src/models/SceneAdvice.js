import Ajv from 'ajv'

import detailsSchema from '@models/schemas/details.schema.json'
import detailSchema from '@models/schemas/detail.schema.json'

const sceneAdviceValidator = new Ajv({ schemas: [detailsSchema, detailSchema], strict: false })

/**
 * @classdesc Model used to create a new preset steps model from a JSON object
 * @memberof models
 */
class SceneAdvice {
  constructor (details) {
    this.details = details
  }

  /**
   * Parses a JSON element to a new scene advice model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let sceneAdvice = null
    // const isValid = sceneAdviceValidator.validate(detailsSchema, json)
    const isValid = true
    if (isValid) {
      const { details } = json
      sceneAdvice = new SceneAdvice(details)
    } else {
      throw new Error(sceneAdviceValidator.errorsText())
    }
    return sceneAdvice
  }
}

export default SceneAdvice
