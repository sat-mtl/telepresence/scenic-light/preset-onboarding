import Ajv from 'ajv'

import stepsSchema from '@models/schemas/steps.schema.json'
import stepSchema from '@models/schemas/step.schema.json'

const presetValidator = new Ajv({ schemas: [stepsSchema, stepSchema], strict: false })

/**
 * @classdesc Model used to create a new preset steps model from a JSON object
 * @memberof models
 */
class Step {
/**
   * Instantiates a new preset step model
   * @param {string} id - ID of the preset step
   * @param {string} title - Title of the preset step
   * @param {string} description - A description of the preset step
   * @param {string} totalPageCount - The total page count of the preset step
   */
  constructor (id, title, description, totalPageCount) {
    this.id = id
    this.title = title
    this.description = description
    this.totalPageCount = totalPageCount
  }

  /**
   * Parses a JSON element to a new preset step
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let step = null

    try {
      const { id, title, description, totalPageCount } = json
      step = new Step(id, title, description, totalPageCount)
    } catch {
      throw new Error(presetValidator.errorsText())
    }

    return step
  }

  /**
   * Parses a JSON element to a new preset steps model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromSteps (json) {
    let steps = []

    const isValid = presetValidator.validate(stepsSchema, json)

    if (isValid) {
      steps = json.steps.map(step => Step.fromJSON(step))
    } else {
      throw new Error(presetValidator.errorsText())
    }

    return steps
  }
}

export default Step
