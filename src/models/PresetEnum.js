/**
 * Enum for all presets
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const PresetTitleEnum = Object.freeze({
  SMALL_HALLS: 'SMALL HALLS',
  BIG_HALLS: 'BIG HALLS'
})

/**
 * Enum for the title of each configuration
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const ConfigurationTitleEnum = Object.freeze({
  VIDEO_ANIMATOR: "Animator's video",
  AUDIO_ANIMATOR: "Animator's audio",
  VIDEO_AUDIENCE: "Audience's video",
  AUDIO_AUDIENCE: "Audience's audio"
})

/**
 * Enum for the id of each configuration
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const ConfigurationIdEnum = Object.freeze({
  VIDEO_ANIMATOR: 'animatorVideo',
  AUDIO_ANIMATOR: 'animatorAudio',
  VIDEO_AUDIENCE: 'audienceVideo',
  AUDIO_AUDIENCE: 'audienceAudio',
  LEFT_SCREEN: 'leftScreen',
  RIGHT_SCREEN: 'rightScreen',
  LEFT_SPEAKER: 'leftSpeaker',
  RIGHT_SPEAKER: 'rightSpeaker'

})
