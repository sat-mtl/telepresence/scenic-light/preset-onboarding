Release Notes
===================

Preset Onboarding 0.1.0 (2022-05-04)
----------------------------------

* ✨ Implement the Checklist component and connect it to the app workflow
* ✨ Implement the PresetSceneSchema component
* ♻ Implement the application cycles in an Enum instead of UI states
* ♻ Improve the demo model by isolating connectors

PresetOnboarding 0.0.0 (2022-03-14)
----------------------------------

- ✨ Implement the test device and advice section for the configuration workflow
- ✨ Implement the stepsSection for the preset workflow
- ✨ Implement the ConfigurationSection
- ✨ Implement a minimal model in order to guide user in preset configuration
- ✨ Implement the page layout
- 👷 Deploy the preset onboarding build to s3
- 💄 Add font styles
- 🎉 Bootstrap the project with ui-components
- 🚀 Add default setup from web-bootstrap
