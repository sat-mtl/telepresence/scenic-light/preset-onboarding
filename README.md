# PresetOnboarding

A micro front-end that aims to guide the user when he uses a telepresence preset.

## Getting started

This project is built using [NodeJS LTS](https://nodejs.org/en/), you should install it with a NodeJS manager such as [NVM](https://github.com/nvm-sh/nvm) or [Volta](https://volta.sh/).

Use the following commands to bootstrap the front-end:

| Command                   | Description                                                      |
|---------------------------|------------------------------------------------------------------|
| `npm install`             | Install all the project dependencies locally                     |
| `npm run build`           | Build and bundle the webapp                                      |
| `npm start`               | Run the webapp in a development server on localhost:8080         |
| `npm run test`            | Run all tests (units and integrations)                           |
| `npm run test:watch`      | Watch all changes and rerun all tests                            |
| `npm run lint`            | Lint the code base with [standardjs](https://standardjs.com/)    |
| `npm run lint:watch`      | Watch all changes and rerun the linter                           |
| `npm run storybook`       | Run the [Storybook](https://storybook.js.org/) on localhost:6006 |
| `npm run storybook:build` | Build and bundle the storybook                                   |


## Support
Please, send us a Bug Resolution Request or a Feature Request in the project's [issue tracker](https://gitlab.com/sat-mtl/telepresence/preset-onboarding/-/issues).

# Authors and acknowledgment
This project was made possible by the [Société des Arts Technologiques (SAT)](http://sat.qc.ca/) and all its [contributors](./AUTHORS.md).

## License
This project is licensed under the GNU General Public License version 3 - see the [LICENSE](./LICENSE.md) file for details.
