const path = require('path')

const rootPath = path.resolve(__dirname, '..')

/**
 * The aliases allow the use of shortcuts in the webapp imports
 * @see [Webpack aliases]{@link https://webpack.js.org/configuration/resolve/#resolvealias}
 * @member {Object} aliases
 */
const aliases = {
  '~': '',
  '@api': 'src/api',
  '@assets': 'assets',
  '@components': 'src/components',
  '@models': 'src/models',
  '@stores': 'src/stores',
  '@styles': 'src/styles',
  '@utils': 'src/utils',

  '@ui-components': path.resolve(rootPath, 'node_modules/@sat-valorisation/ui-components')
}

/**
 * Adapts aliases for Webpack
 * @param {Object} aliases - All aliases to adapt
 * @param {string} rootDir - Path of the rootDir
 * @returns {Object} All aliases for Webpack
 */
function getWebpackAliases (aliases, rootDir) {
  return Object.fromEntries(
    Object.entries(aliases).map(
      ([key, value]) => [key, `${rootDir}/${value}`]
    )
  )
}

/**
 * Adapts aliases for Jest
 * @see [Jest moduleNameMapper configuration]{@link https://jestjs.io/docs/configuration#modulenamemapper-objectstring-string--arraystring}
 * @param {Object} aliases - All aliases to adapt
 * @returns {Object} All aliases for Jest
 */
function getJestAliases (aliases) {
  return Object.fromEntries(
    Object.entries(aliases).map(
      ([key, value]) => [`^${key}(.*)`, `<rootDir>/${value}$1`]
    )
  )
}

module.exports = {
  srcAliases: getWebpackAliases(aliases, rootPath),
  testAliases: getJestAliases(aliases)
}
