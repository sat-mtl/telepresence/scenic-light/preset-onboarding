// For a detailed explanation regarding each configuration property
// visit: https://jestjs.io/docs/en/configuration.html

const path = require('path')
const { testAliases } = require('./alias.config.js')

/**
 * The Jest configuration that runs all tests
 * @see [The Jest configuration]{@link https://jestjs.io/docs/configuration}
 * @member {Object} jestConfig
 */
const jestConfig = {
  rootDir: path.resolve(__dirname, '..'),

  setupFiles: [
    '<rootDir>/config/jest.setup.js'
  ],

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: './coverage/',

  collectCoverageFrom: [
    'src/**/*.js'
  ],

  // An array of regexp pattern strings, matched against all module paths
  // before considered 'visible' to the module loader
  modulePathIgnorePatterns: [
    'node_modules'
  ],

  // A map from regular expressions to module names that allow to stub out resources with a single module
  // Be careful, the order of the keys is a priority order
  moduleNameMapper: {
    '\\.svg$': '<rootDir>/test/utils/emptyComponent.js',
    '\\.(css|scss|jpg|png)$': '<rootDir>/test/utils/emptyModule.js',
    ...testAliases
  },

  testEnvironment: 'jsdom'
}

module.exports = jestConfig
